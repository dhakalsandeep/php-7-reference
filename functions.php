<?php
    function dd($variable){
        echo '<pre>';
        die(var_dump($variable));
        echo '</pre>';
    }
    function message($message, $tag = '') {
        if ($tag) {
            return "<{$tag}>{$message}</{$tag}> <br>";    
        }
        return "{$message} <br>";
    }
    function hr() {
        return "<hr>";
    }
    function hr2() {
        return "<hr><hr>";
    }  

?>