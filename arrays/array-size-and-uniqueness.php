
<?php require_once('functions.php'); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></link>

    <title>PHP Professional</title>
  </head>
  <body>
    <div class="container">
        <?php
            echo message('<i>Determining Array Size and Uniqueness</i>', 'strong');
            echo hr();
            echo message('Size of an Array', 'strong');
            echo hr();
            echo message('integer count(array array [, int mode])', 'strong');
            echo hr2();
            $garden = array("cabbage", "peppers", "turnips", "carrots");
            echo message('$garden = array("cabbage", "peppers", "turnips", "carrots");');
            echo message('count($garden)');
            echo message(count($garden));

            echo hr();

            $locations = array("Italy", "Amsterdam", array("Boston","Des Moines"), "Miami");
            echo message('$locations = array("Italy", "Amsterdam", array("Boston","Des Moines"), "Miami");');
            echo message('count($locations, 1)');
            echo message(count($locations, 1));

            echo message('<i>It may be confusing to see this result as the array has only five elements, but the array holding the three elements is also counted as 1</i>');

            echo message('Note    the sizeof() function is an alias of count(). It is functionally identical', 'strong');
            echo hr2();


            echo message('<i>Counting Array Value Frequency</i>', 'strong');
            echo hr();
            echo message('array array_count_values(array array)', 'strong');
            echo hr2();
            $states = ["Ohio", "Iowa", "Arizona", "Iowa", "Ohio"];
            echo message('$states = ["Ohio", "Iowa", "Arizona", "Iowa", "Ohio"];');
            $stateFrequency = array_count_values($states);
            echo message('$stateFrequency = array_count_values($states);');
            print_r($stateFrequency);
            echo hr2();

            echo message('<i>Determining Unique / Distinct Array Values</i>', 'strong');
            echo hr();
            echo message('array array_unique(array array [, int sort_flags = SORT_STRING])', 'strong');
            echo hr2();
            echo message('$states = ["Ohio", "Iowa", "Arizona", "Iowa", "Ohio"];'); 
            $uniqueStates = array_unique($states);
            echo message('$uniqueStates = array_unique($states);'); 
            echo '$uniqueStates = ';
            print_r($uniqueStates);

            echo hr2();

        ?>
        
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>