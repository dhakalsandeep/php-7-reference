
<?php require_once('functions.php'); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></link>

    <title>PHP Professional</title>
  </head>
  <body>
    <div class="container">
        <?php 
            echo message('<i>Traversing Arrays</i>','strong');
            echo hr();
            // Retrieving Current Array Key
            echo message('Retrieving Current Array Key','strong');
            echo hr2();
            echo message('key(array array)','strong');

            $capitals = array("Ohio" => "Columbus", "Iowa" => "Des Moines");
            print_r($capitals);
            echo '<br>';
            echo message("Can you name the capitals of these states?");
            echo '<br>';
            echo message('print_r(key($capitals));');
            echo '<br>';
            print_r(key($capitals));
            echo '<br>';
            echo message('next($capitals);');
            echo '<br>';
            next($capitals);
            echo message('print_r(key($capitals));');
            echo '<br>';
            print_r(key($capitals));
            echo '<br>';
            echo message('key() function returns the key located at the current pointer position of the provided array','strong');
            echo hr2();


            // Retrieving Current Array Value
            echo message('Retrieving Current Array Value','strong');
            echo hr2();
            echo message('current(array array)','strong');

            $capitals = array("Ohio" => "Columbus", "Iowa" => "Des Moines");
            print_r($capitals);
            echo '<br>';
            echo message("Can you name the states belonging to these capitals?");
            echo '<br>';
            echo message('print_r(current($capitals));');
            echo '<br>';
            print_r(current($capitals));
            echo '<br>';
            echo message('next($capitals);');
            echo '<br>';
            next($capitals);
            echo message('print_r(current($capitals));');
            echo '<br>';
            print_r(current($capitals));
            echo '<br>';
            echo message('current() function returns the array value residing at the current pointer position of the array','strong');
            echo hr2();

            // Moving the Array Pointer
            echo message('<i>Moving the Array Pointer</i>','strong');
            echo hr();
            // Moving the Pointer to the Next Array Position
            echo message('Moving the Pointer to the Next, Precious, Start and End of Array','strong');
            echo hr2();
            $fruits = array("apple", "orange", "banana", "mango");
            print_r($fruits);
            echo '<br>';
            echo message('Current Position :');
            echo message(current($fruits), 'strong');
            $fruit = next($fruits);
            echo message('next($fruits) Position :');
            echo message($fruit,'strong');
            $fruit = end($fruits);
            echo message('end($fruits) Position :');
            echo message($fruit,'strong');
            $fruit = prev($fruits);
            echo message('prev($fruits) Position :');
            echo message($fruit,'strong');
            $fruit = reset($fruits);
            echo message('reset($fruits) Position :');
            echo message($fruit,'strong');
            echo hr2();

            // Passing Array Values to a Function
            echo message('<i>Passing Array Values to a Function</i>','strong');
            echo hr();
            // array_walk()
            echo message('array_walk(array &array, callback function [, mixed $userdata])','strong');
            echo hr2();

            $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");

            echo message('$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");');

            function test_alter(&$item1, $key, $prefix)
            {
                $item1 = "$prefix: $item1";
            }

            function test_print_walk($item2, $key)
            {
                echo message("$key. $item2");
            }

            echo message("Before ...:");
            echo message('array_walk($fruits, "test_print_walk");');
            echo '<br><pre>
    function test_print_walk($item2, $key)
    {
        echo message("$key. $item2");
    }
            </pre>';
            array_walk($fruits, 'test_print_walk');

            

            echo message('array_walk($fruits, "test_alter", "fruit");');
            echo '<br><pre>
    function test_alter(&$item1, $key, $prefix)
    {
        $item1 = "$prefix: $item1";
    }
            </pre>';
            array_walk($fruits, 'test_alter', 'fruit');
            print_r($fruits);
            echo '<br>';
            echo message('array_walk($fruits, "test_print_walk");');
            echo message("... and after:");

            array_walk($fruits, 'test_print_walk');

            echo message('array_walk() function will pass each element of an array to the user-defined function','strong');
            echo hr2();

            // array_walk_recursive()
            echo message('array_walk_recursive(array &array, callback function [, mixed $userdata])','strong');
            echo hr2();
            echo message('$sweet = array("a" => "apple", "b" => "banana");');
            $sweet = array('a' => 'apple', 'b' => 'banana');

            echo message('$fruits = array("sweet" => $sweet, "sour" => "lemon");');
            $fruits = array('sweet' => $sweet, 'sour' => 'lemon');

            

            echo '<br><pre>
    function test_print($item, $key)
    {
        echo message("$key holds $item");
    }
            </pre>';

            echo message('array_walk_recursive($fruits, "test_print");');

            array_walk_recursive($fruits, 'test_print');

            function test_print($item, $key)
            {
                echo message("$key holds $item");
            }

            echo message('array_walk_recursive() is capable to recursively apply a user-defined function to every element in an array','strong');
            echo hr2();

            // array_map()
            echo message(' array_map ( callable|null $callback , array $array , array ...$arrays ) : array','strong');
            echo hr2();
            function cube($n)
            {
                return ($n * $n * $n);
            }

            echo message('$a = [1, 2, 3, 4, 5];');
            $a = [1, 2, 3, 4, 5];
            
            echo message('$b = array_map("cube", $a);');
            echo '<pre>
    function cube($n)
    {
        return ($n * $n * $n);
    }
            </pre>';
            $b = array_map('cube', $a);
            echo message('// Result');
            echo '$b = ';
            print_r($b);
            echo '<br>';

            echo message('array_map() returns an array containing the results of applying the callback function to the corresponding index of array','strong');
            echo hr2();
        ?>
        
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>