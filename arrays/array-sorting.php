
<?php require_once('functions.php'); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></link>

    <title>PHP Professional</title>
  </head>
  <body>
    <div class="container">
        <?php
            echo message('<i>Array Sorting</i>','strong');
            echo hr();
            echo message('array array_reverse(array array [, boolean preserve_keys])', 'strong');
            echo hr2();

            echo message('$states = array("Delaware", "Pennsylvania", "New Jersey");');
            $states = array("Delaware", "Pennsylvania", "New Jersey");
            $sortStates = array_reverse($states);
            echo message('$sortStates = array_reverse($states);');
            echo '$sortStates = ';
            print_r($sortStates);

            echo hr();

            echo message('$sortStates = array_reverse($states,1);');
            $sortStates = array_reverse($states,1);
            echo '$sortStates = ';
            print_r($sortStates);
            echo hr();
            echo message('Note: <i>Arrays with associative keys are not affected by preserve_keys; key mappings are always preserved in this case.</i>','strong');
            echo hr2();

            // Flipping an Array
            echo message('<i>Flipping Array Keys and Values</i>','strong');
            echo hr();
            echo message('array array_flip(array array)', 'strong');
            echo hr2();

            echo message('$state = array(0 => "Delaware", 1 => "Pennsylvania", 2 => "New Jersey");');
            $state = array(0 => "Delaware", 1 => "Pennsylvania", 2 => "New Jersey");
            $flipStates = array_flip($state);
            echo message('$flipStates = array_reverse($state);');
            echo '$flipStates = ';
            print_r($flipStates);

            echo hr();
            echo message('<i>array_flip() function reverses the roles of the keys and their corresponding values in an array</i>','strong');

            echo hr2();

            // Sorting an Array

            echo message('<i>Sorting  an Array</i>','strong');
            echo hr();
            echo message('void sort(array array [, int sort_flags])', 'strong');
            echo hr2();

            echo message('$grades = array(42, 98, 100, 100, 43, 12);');
            $grades = array(42, 98, 100, 100, 43, 12);
            echo message('sort($grades)');
            sort($grades);
            echo '$grades = ';
            print_r($grades);
            echo hr();
            // asort()
            echo message('void asort(array array [, int sort_flags])', 'strong');
            echo hr();
            echo message('$grades = array(42, 98, 100, 100, 43, 12);');
            $grades = array(42, 98, 100, 100, 43, 12);
            echo message('asort($grades)');
            asort($grades);
            echo '$grades = ';
            print_r($grades);


            echo hr();
            // rsort()
            echo message('void rsort(array array [, int sort_flags])', 'strong');
            echo hr();
            echo message('$grades = array(42, 98, 100, 100, 43, 12);');
            $grades = array(42, 98, 100, 100, 43, 12);
            echo message('rsort($grades)');
            rsort($grades);
            echo '$grades = ';
            print_r($grades);


            echo hr();
            // arsort()
            echo message('void arsort(array array [, int sort_flags])', 'strong');
            echo hr();
            echo message('$grades = array(42, 98, 100, 100, 43, 12);');
            $grades = array(42, 98, 100, 100, 43, 12);
            echo message('arsort($grades)');
            arsort($grades);
            echo '$grades = ';
            print_r($grades);


            echo hr();
            
            echo message('<i>sort() function sorts an array, ordering elements from lowest to highest value without maintaining key.</i>','strong');
            echo message('<i>asort() function sorts an array, ordering elements from lowest to highest value maintaining key values.</i>','strong');
            echo message('<i>rsort() function sorts an array, ordering elements from highest to lowest value without maintaining key.</i>','strong');
            echo message('<i>arsort() function sorts an array, ordering elements from highest to lowest value maintaining key.</i>','strong');
            echo message('<i>ksort() function sorts an array by its keys, returning TRUE on success and FALSE otherwise.</i>','strong');
            echo message('<i>krsort() function operates identically to ksort(), sorting by key, except that it sorts in reverse (descending) order.</i>','strong');
            echo message('Note : <i>There are other types like natsort(), natcasesort() and usort() which are not normally used. Learn about them if you need them.</i>','strong');

            echo hr2();

        ?>
        
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>