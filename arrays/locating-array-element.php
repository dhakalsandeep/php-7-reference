
<?php require_once('functions.php'); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></link>

    <title>PHP Professional</title>
  </head>
  <body>
    <div class="container">
        <?php 

            // Single Dimension Array Non Associative 
            echo message('<i>Searching an Array</i>','strong');
            echo hr2();
            echo message('in_array(needle, haystack [, boolean strict])','strong');
            $state = "Ohio";
            echo message('$state = "Ohio"');
            echo message('$states = ["California", "Hawaii", "Ohio", "New York"]');
            $states = ["California", "Hawaii", "Ohio", "New York"];

            echo message('if(in_array($state, $states))','i');

            if(in_array($state, $states)) { 
                echo message("Not to worry, $state is found!");
            }

            echo message('in_array() function searches an array for a specific value, returning TRUE if the value is found and FALSE otherwise','strong');
            echo hr2();

            // Searching Associative Array Key

            $state = [];

            echo message('<i>Searching Associative Array Keys</i>','strong');
            echo hr2();
            echo message('array_key_exists(key, array)','strong');
            $state["Delaware"] = "December 7, 1787";
            $state["Pennsylvania"] = "December 12, 1787";
            $state["Ohio"] = "March 1, 1803";
            echo '$state = ';
            print_r($state);
            echo '<br>';
            echo message('if (array_key_exists("Ohio", $state))');
            if (array_key_exists("Ohio", $state))   
                printf("Ohio joined the Union on %s", $state["Ohio"]);

            echo '<br>';
            echo message('array_key_exists() returns TRUE if a specified key is found in an array and FALSE otherwise','strong');
            echo hr2();


            // Searching Associative Array values

            $state = [];

            echo message('<i>Searching Associative Array Values</i>','strong');
            echo hr2();
            echo message('array_search(needle, haystack [, boolean strict])','strong');
            $state["Ohio"] = "March 1";
            $state["Delaware"] = "December 7";
            $state["Pennsylvania"] = "December 12";
            echo '$state = ';
            print_r($state);
            echo '<br>';
            $founded = array_search("December 7", $state);
            echo message('array_search("December 7", $state) returns '.$founded);
            if ($founded) 
                printf("%s was founded on %s.", $founded, $state[$founded]);
            echo '<br>';
            echo message('array_search() function searches an array for a specified value, returning its key if located and FALSE otherwise','strong');
            echo hr2();


            // Retrieving Array Keys

            $state = [];

            echo message('<i>Retrieving Array Keys</i>','strong');
            echo hr2();
            echo message('array_keys(array array [, mixed search_value [, boolean strict]])','strong');
            $state["Ohio"] = "March 1";
            $state["Delaware"] = "December 7";
            $state["Pennsylvania"] = "December 12";
            echo '$state = ';
            print_r($state);
            echo '<br>';
            $keys = array_keys($state);
            echo '$keys = ';
            print_r($keys);
            echo '<br>';
            echo message('array_keys() function returns an array consisting of all keys located in an array','strong');
            echo hr2();

            // Retrieving Array Values

            $state = [];

            echo message('<i>Retrieving Array Keys</i>','strong');
            echo hr2();
            echo message('array_values(array array)','strong');
            $state["Ohio"] = "March 1";
            $state["Delaware"] = "December 7";
            $state["Pennsylvania"] = "December 12";
            echo '$state = ';
            print_r($state);
            echo '<br>';
            $values = array_values($state);
            echo '$values = ';
            print_r($values);
            echo '<br>';
            echo message('array_values() function returns all values located in an array','strong');
            echo hr2();


            // Extracting Columns

            echo message('<i>Extracting Columns</i>','strong');
            echo hr2();
            echo message('array = array_column(array array, mixed column_key [, mixed index_key = null] )','strong');
            $persons = [  
                ['name' => 'Homer Simpson', 'gender' => 'Male'],
                ['name' => 'Marge Simpson', 'gender' => 'Female'],
                ['name' => 'Bart Simpson', 'gender' => 'Male']
            ];

            
            echo '$persons = Array (<br>';
            foreach($persons as $person) {
                print_r($person); 
                echo '<br>';   
            }
            echo message(')');
            $names = array_column($persons, 'name');
            echo message('$names = array_column($persons, "name")');
            echo '$names = ';
            print_r($names);
            echo '<br>';
            echo message('array_column() will return an indexed array containing only the values from the specified column','strong');
            echo hr2();
        ?>
        
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>