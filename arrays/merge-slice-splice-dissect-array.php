
<?php require_once('functions.php'); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></link>

    <title>PHP Professional</title>
  </head>
  <body>
    <div class="container">
        <?php
            echo message('<i>Merging, Slicing, Splicing, and Dissecting Arrays</i>','strong');
            echo hr();
            echo message('array array_merge(array array1, array array2 [, array arrayN])', 'strong');
            echo hr2();

            echo message('$letters = array("A","B","C","D","E");');
            $letters = array("A","B","C","D","E");
            echo message('$numbers = array(1,2,3,4,5);');
            $numbers = array(1,2,3,4,5);
            echo message('$all = array_merge($letters,$numbers);');
            $all = array_merge($letters,$numbers);
            echo '$all = ';
            print_r($all);
            echo '<br>';
            echo message('shuffle($all);');
            shuffle($all);
            echo '$all = ';
            print_r($all);
            echo hr();
            echo message('<i>array_merge() function merges arrays together, returning a single, unified array.</i>','strong');
            echo hr2(); 

            echo message('array array_merge_recursive(array array1, array array2 [, array arrayN])', 'strong');
            echo hr2();

            echo message('$class1 = array(<strong>"John" => 100</strong>, "James" => 85);');
            $class1 = array("John" => 100, "James" => 85);
            echo message('$class2 = array("Micky" => 78, <strong>"John" => 45</strong>);');
            $class2 = array("Micky" => 78, "John" => 45);
            echo message('$classScores = array_merge_recursive($class1, $class2);');
            $classScores = array_merge_recursive($class1, $class2);
            echo '$classScores = ';
            print_r($classScores);
            echo hr();
            echo message('<i>array_merge_recursive() function operates identically to array_merge(), 
            joining two or more arrays together to form a single, unified array. 
            The difference between the two functions lies in the way that this function behaves 
            when a string key located in one of the input arrays already exists within the resulting array.</i>','strong');
            echo hr2(); 

            echo message('array array_combine(array keys, array values)', 'strong');
            echo hr2();

            echo message('$abbreviations = array("AL", "AK", "AZ", "AR");');
            $abbreviations = array("AL", "AK", "AZ", "AR");
            echo message('$states = array("Alabama", "Alaska", "Arizona", "Arkansas");');
            $states = array("Alabama", "Alaska", "Arizona", "Arkansas");
            echo message('$stateMap = array_combine($abbreviations,$states);');
            $stateMap = array_combine($abbreviations,$states);
            echo '$stateMap = ';
            print_r($stateMap);
            echo hr();
            echo message('<i>array_combine() function produces a new array consisting of a submitted set of keys and corresponding values.</i>','strong');
            echo hr2(); 


            echo message('array array_slice(array array, int offset [, int length [, boolean preserve_keys]])', 'strong');
            echo hr2();

            echo message('$states = array("Alabama", "Alaska", "Arizona", "Arkansas","California", "Colorado", "Connecticut");');
            $states = array("Alabama", "Alaska", "Arizona", "Arkansas","California", "Colorado", "Connecticut");
            echo message('$subset = array_slice($states, 4, 2, 1);');
            $subset = array_slice($states, 4,2,1);
            echo '$subset = ';
            print_r($subset);
            echo hr();
            echo message('<i>array_slice() function returns a section of an array based on a starting offset and length value.</i>','strong');
            echo hr2(); 


            echo message('array array_splice(array array, int offset [, int length [, array replacement]])', 'strong');
            echo hr2();

            echo message('$states = array("Alabama", "Alaska", "Arizona", "Arkansas","California", "Colorado", "Connecticut");');
            $states = array("Alabama", "Alaska", "Arizona", "Arkansas","California", "Colorado", "Connecticut");
            echo message('$subset = array_splice($states, 4, -1);');
            $subset = array_splice($states, 4, 2);
            echo '$subset = ';
            print_r($subset);
            echo '<br>';
            echo '$states = ';
            print_r($states);
            echo hr();
            echo message('<i>array_splice() function removes all elements of an array found within a specified range, replacing them with values identified by the replacement parameter and returns the removed elements in the form of an array.</i>','strong');
            echo hr2(); 


            echo message('array array_intersect(array array1, array array2 [, arrayN])', 'strong');
            echo hr2();

            echo message('$array1 = array("OH", "CA", "NY", "HI", "CT");');
            $array1 = array("OH", "CA", "NY", "HI", "CT");
            echo message('$array2 = array("OH", "CA", "HI", "NY", "IA");');
            $array2 = array("OH", "CA", "HI", "NY", "IA");
            echo message('$array3 = array("TX", "MD", "NE", "OH", "HI");');
            $array3 = array("TX", "MD", "NE", "OH", "HI");
            echo message('$intersection = array_intersect($array1, $array2, $array3);');
            $intersection = array_intersect($array1, $array2, $array3);
            echo '$intersection = ';
            print_r($intersection);
            echo hr();
            echo message('<i>array_intersect() function returns a key-preserved array consisting only of those values present in the first array that are also present in each of the other input arrays.</i>','strong');
            echo message('<i>Consult PHP Manual for below if necessary</i>','strong');
            echo message('<i>array_intersect_key(), array_intersect_ukey(), array_intersect_assoc()</i>','strong');
            echo message('<i>array_diff(), array_diff_key(), array_diff_ukey(), array_diff_assoc()</i>','strong');
            echo message('<i>array_udiff_assoc(), array_udiff_uassoc(), and array_diff_uassoc()</i>','strong');
            echo hr2(); 
        ?>
        
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>