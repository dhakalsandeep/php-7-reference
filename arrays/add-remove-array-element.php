<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></link>

    <title>PHP Professional</title>
  </head>
  <body>
    <div class="container">
		<?php
            function message($message, $tag = '') {
                if ($tag) {
                    return "<{$tag}>{$message}</{$tag}> <br>";    
                }
                return "{$message} <br>";
            }
            function hr() {
                return "<hr>";
            }
            function hr2() {
                return "<hr><hr>";
            }

            // array_unshift()
            echo message('array_unshift() :','strong');
            echo hr();
            $states = array("Ohio", "New York");
            echo message("Original States Array",'i');
            print_r($states);
            echo hr();
            array_unshift($states, "California", "Texas");
            echo message('Array after array_unshift($states, "California", "Texas")','i');
            print_r($states);
            // $states = array("California", "Texas", "Ohio", "New York");
            echo hr();

            echo message("array_unshift() puts the value in the beginning of array",'strong');
            echo hr2();

            // array_push()
            echo message('array_push() :','strong');
            echo hr();
            array_push($states, "Dallas", "Louisiana");
            print_r($states);

            echo hr();

            echo message("array_shift() puts the value in the end of array",'strong');
            echo hr2();

            // array_shift()
            echo message('array_shift() :','strong');
            echo hr();
            $state = array_shift($states);
            echo '$state = '.$state;
            echo hr();

            echo '$states = ';
            print_r($states);

            echo hr();

            echo message("array_shift() removes and returns the first element found in an array",'strong');
            echo hr2();

            // array_pop()
            echo message('array_pop() :','strong');
            echo hr();
            $state = array_pop($states);
            echo '$state = '.$state;
            echo hr();

            echo '$states = ';
            print_r($states);

            echo hr();

            echo message("array_pop() removes and returns the last element found in an array",'strong');
            echo hr2();

        ?>
        
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>