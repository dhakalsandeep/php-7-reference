# php-7-reference

Step-1
- git checkout branch-name

Step-2
- php -S localhost:8000

Branches :
- output-array
- add-remove-array-element
- locating-array-element
- traversing-arrays
- array-size-and-uniqueness
- array-sorting
- merge-slice-splice-dissect-array